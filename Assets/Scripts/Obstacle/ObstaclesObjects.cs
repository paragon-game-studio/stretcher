using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class ObstaclesObjects : MonoBehaviour
{
    public bool obstacle_1;
    public bool obstacle_4;

    AnimancerComponent obComp;
    AnimationClip ob4;
   
    private void PlayAnim()
    {
        var state = obComp.Play(AnimationManager.Instance.anim.obAnim, .25f);
        state.Speed = 1;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Patient") && obstacle_1 && Player.Instance.gameObject.CompareTag("Player"))
        {
            AnimationManager.Instance.CameraShape(AnimationManager.Instance.anim.cameraShape2);
            GetComponent<Collider>().enabled = false;
            ObstaclesManager.Instance.Obstacle1(other.gameObject);
        }
        else if (other.CompareTag("Patient") && obstacle_4 && Player.Instance.gameObject.CompareTag("Player"))
        {
            AnimationManager.Instance.CameraShape(AnimationManager.Instance.anim.cameraShape2);
            obComp = gameObject.transform.GetChild(2).GetComponent<AnimancerComponent>();
            PlayAnim();
            ObstaclesManager.Instance.Obstacle4(other.gameObject, gameObject);
        }
    }
}
