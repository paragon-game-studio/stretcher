using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Animancer;

public class Stretcher : LocalSingleton<Stretcher>
{
    public StretcherInput _stretcher;


    public void SPStart(GameObject patient)
    {
        //GameObject patient = pat.transform.parent.gameObject;
        _stretcher.positionPlus = (Player.Instance.player.patient.Count);
        patient.transform.SetParent(_stretcher.stretcher.transform);
        patient.transform.tag = "Patient";
        patient.transform.GetComponent<Rigidbody>().isKinematic = true;
        StartCoroutine(PathMove(patient));
    }
    IEnumerator PathMove(GameObject patient)
    {
        for (int i = 0; i < _stretcher.path.Length; i++)
        {
            patient.transform.DOMove(_stretcher.path[i].transform.position, .15f).SetEase(Ease.Linear);
            if (i == _stretcher.path.Length-1)
            {
                _stretcher.pathTransform.transform.DOLocalMoveY(_stretcher.zeroPosition.y + (_stretcher.positionPlus /3f), _stretcher.speed);
                patient.transform.DOLocalMove(_stretcher.zeroPosition + new Vector3(0, (_stretcher.positionPlus / 2) + _stretcher.plus, 0), _stretcher.speed).SetEase(Ease.Linear).OnComplete(() =>
                {
                    //Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.GetChild(0).GetComponent<AnimancerComponent>().Stop();
                    //StretcherMove(gameObject.transform.GetChild(0).gameObject);
                });
            }
            yield return new WaitForSeconds(.08f);
        }
    }

    void StretcherMove(GameObject st)
    {
        float time = .5f;
        float value = -.5f;
        st.transform.DOLocalMoveY(value, time);
        st.transform.DOLocalMoveY(.3f, time).SetDelay(time);
    }
    public void SteretcherRun() 
    {
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.stretcher, AnimationManager.Instance.anim.run);
    }
    public void SteretcherHit() 
    {
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.stretcher, AnimationManager.Instance.anim.hit);
    }

}
[System.Serializable]
public class StretcherInput
{
    //Stretcher
    public GameObject[] path;
    public GameObject pathTransform;


    public Vector3 zeroPosition;

    public float positionPlus;
    public float plus;
    public float speed;

    public GameObject stretcher;


}
