using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class PatientAnim : MonoBehaviour
{
    public bool ragdoll;
    AnimancerComponent animancer;
    [SerializeField] private AnimationClip[] wriggling;
    Rigidbody[] _rb;
    void Start()
    {
        if (!ragdoll)
        {
           // _rb = GetComponentsInChildren<Rigidbody>();
            animancer = gameObject.transform.GetChild(0).GetComponent<AnimancerComponent>();
            int random = Random.Range(0, 2);
            var state = animancer.Play(wriggling[random], .25f);
            state.Speed = 1;
            //RagdollActive(true);
        }
    }

    void Vait()
    {
       
    }
    public void RagdollActive(bool value)
    {
        gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>().enabled = value;
        foreach (Rigidbody rb in _rb)
        {
            rb.isKinematic = value;
        }
    }
}
