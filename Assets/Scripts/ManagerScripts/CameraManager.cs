using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraManager : LocalSingleton<CameraManager>
{
    public Transform target;
    public Transform camPos1;
    public Transform camPos2;
    public Transform camPos3;
    public Transform camPos4;
    public Vector3 offset2;
    public Quaternion rot;
    public float followSmooth;
    private void Start()
    {
        target = camPos2;
    }
    private void FixedUpdate()
    {
        CamMove();
    }
    
    public void CamMove()
    {
        transform.position = Vector3.Slerp(transform.position, target.position, followSmooth);
        transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, followSmooth);

        //if (Player.Instance.player.patient.Count > 5)
        //{
        //    target = camPos2;
        //}
        //else if (Player.Instance.player.patient.Count > 10)
        //{
        //    target = camPos3;
        //}
        //else
        //{
        //    target = camPos1;
        //}
    }
}
