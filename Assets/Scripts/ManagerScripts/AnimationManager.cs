using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class AnimationManager : LocalSingleton<AnimationManager>
{
    public Animation anim;

    private void Start()
    {
        anim.camera = Camera.main.GetComponent<AnimancerComponent>();
        PlayAnim(anim.stretcher,anim.run);
    }
    public void PlayAnim(AnimancerComponent comp, AnimationClip clip, float speed = 1,float fade = .25f)
    {
        var state = comp.Play(clip, fade);
        state.Speed = speed;
    }
    public void CameraShape(AnimationClip clip)
    {
        PlayAnim(anim.camera, clip);
    }
}
[System.Serializable]
public class Animation
{
    public AnimancerComponent camera;
    public AnimancerComponent stretcher;
    public AnimancerComponent doctor1;
    public AnimancerComponent doctor2;
    public AnimationClip cameraShape;
    public AnimationClip cameraShape2;
    public AnimationClip doctorRun;
    public AnimationClip doctorIdle;
    public AnimationClip run;
    public AnimationClip hit;
    public AnimationClip obAnim;
}
