using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameManager : LocalSingleton<GameManager>
{
   public void GameStart() 
   {
        Player.Instance.PlayerRun();
        Stretcher.Instance.SteretcherRun();
        PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed;
   } 
    public void GameLose() 
    {
        Player.Instance.gameObject.GetComponent<PlayerMove>().enabled = false;
        Camera.main.GetComponent<Animator>().enabled = false;
        UIManager.Instance.LosePanel();
        Player.Instance.PlayerIdle();
        AnimationManager.Instance.anim.stretcher.Stop();
        Stretcher.Instance.gameObject.transform.DORotate(new Vector3(0, 0, 0), .5f);
        PlayerMove.Instance.move.speed = 0;
    } 
   public void GameFinish() 
   {
        Player.Instance.gameObject.GetComponent<PlayerMove>().enabled = false;
        Player.Instance.finish = true;
        Player.Instance.player.speedForce.Stop();
        UIManager.Instance.FinishPanel();
        Player.Instance.PlayerIdle();
        AnimationManager.Instance.anim.stretcher.Stop();
        Stretcher.Instance.gameObject.transform.DORotate(new Vector3(0, 0, 0), .5f);
        PlayerMove.Instance.move.speed = 0;
   }





}


