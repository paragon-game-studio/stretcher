using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIManager : LocalSingleton<UIManager>
{
    public UIElement u�;
    #region
    public ProggesBar proggesBar;
    private static int _currlevel = 1;
    public static int currLevel
    {
        get
        {
            return PlayerPrefs.GetInt("currLevel", _currlevel);
        }
        set
        {
            PlayerPrefs.SetInt("currLevel", value);
            _currlevel = value;
        }
    }
    void Start()
    {
        u�.levelText.text = currLevel.ToString();
        u�.fever = true;
        proggesBar.fullDistance = GetDistance();
    }
    private void FixedUpdate()
    {
        float newDistance = GetDistance();
        float progressValuve = Mathf.InverseLerp(proggesBar.fullDistance, 0, newDistance);
        UpdateProgressFill(progressValuve);

    }
    void UpdateProgressFill(float valuve)
    {
        proggesBar.proggresBar.fillAmount = valuve;


    }
    private float GetDistance()
    {
        return Vector3.Distance(proggesBar.player.transform.position, proggesBar.finishFlag.transform.position);
    }
    #endregion
    #region
    public void Finish()
    {
        u�.fever = true;
        Player.Instance.player.heal.Stop();
        proggesBar.proggres.transform.DOScale(Vector3.zero, u�.u�ScaleDownSpeed).SetEase(Ease.InBack);
        u�.feverParent.transform.DOScale(Vector3.zero, u�.u�ScaleDownSpeed).SetEase(Ease.InBack);
    }
    public void FinishPanel()
    {
        u�.finishPanel.transform.DOScale(Vector3.one, u�.u�ScaleDownSpeed).SetEase(Ease.OutBack);
    }
    public void LosePanel()
    {
        proggesBar.proggres.gameObject.SetActive(false);
        u�.feverParent.gameObject.SetActive(false);
        u�.losePanel.transform.DOScale(Vector3.one, u�.u�ScaleDownSpeed).SetEase(Ease.OutBack);
    }
    public void NextButton()
    {
        _currlevel++;
        SceneManager.LoadScene("Level" + (((currLevel - 1) % SDKManager.maxLevelCount) + 1));
    }
    #endregion
    #region
    public void TapToStart()
    {
        u�.gamePlay = true;
        GameManager.Instance.GameStart();
        proggesBar.proggres.transform.DOScale(Vector3.one, u�.u�ScaleDownSpeed).SetEase(Ease.OutBack);
        u�.tapToStart.transform.DOScale(Vector3.zero, u�.u�ScaleDownSpeed).SetEase(Ease.InBack).OnComplete(() =>
        {
            u�.startPanel.transform.DOScale(Vector3.zero, u�.u�ScaleDownSpeed);
        });
    }
    public void ResetButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    #endregion

    public void FeverInvoke()
    {
        if (u�.fever)
        {
            StartCoroutine(FeverBar());
        }

    }
    IEnumerator FeverBar()
    {
        for (int i = 0; i < 15; i++)
        {
            yield return new WaitForSeconds(.05f);
            u�.feverBar.fillAmount += .007f;
            if (u�.feverBar.fillAmount >= 1)
            {
                u�.fever = false;
                StartCoroutine(BarScale());
                Player.Instance.player.speedForce.Play();
                Player.Instance.player.heal.Play();
                Player.Instance.gameObject.tag = "Untagged";
                PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed + 5;
                while (!u�.fever)
                {
                    yield return new WaitForSeconds(.03f);
                    u�.feverBar.fillAmount -= .005f;
                    if (u�.feverBar.fillAmount <= 0)
                    {
                        Player.Instance.player.speedForce.Stop();
                        Player.Instance.player.heal.Stop();
                        Player.Instance.gameObject.tag = "Player";
                        PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed;
                        u�.fever = true;
                        u�.feverBar.gameObject.transform.DOScale(Vector3.one, .3f).SetEase(Ease.Linear);
                        break;
                    }
                }



            }
        }

    }
    IEnumerator BarScale()
    {
        while (!u�.fever)
        {
            u�.feverParent.transform.DOScale(new Vector3(.75f, .75f, .75f), .2f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(.2f);
            u�.feverParent.transform.DOScale(Vector3.one, .2f).SetEase(Ease.Linear);
            yield return new WaitForSeconds(.2f);
        }

    }
        public void TMPXCount(string x)
        {
            u�.xCount.text = "X" + x;
            u�.xCount.gameObject.transform.DOScale(Vector3.one, .4f).SetEase(Ease.Linear);
            u�.xCount.gameObject.transform.DOScale(Vector3.zero, .3f).SetDelay(.4f).SetEase(Ease.Linear);
        }
    }
    [System.Serializable]
    public class ProggesBar
    {
        public Image proggresBar;
        public GameObject proggres;
        public GameObject player;
        public GameObject finishFlag;
        public float fullDistance;
    }
    [System.Serializable]
    public class UIElement
    {
        public float u�ScaleDownSpeed;
        public GameObject startPanel;
        public GameObject tapToStart;
        public GameObject finishPanel;
        public GameObject losePanel;
        public TextMeshProUGUI xCount;
        public TextMeshProUGUI levelText;
        public bool gamePlay;
        public GameObject feverParent;
        public Image feverBar;
        public bool fever;

    }