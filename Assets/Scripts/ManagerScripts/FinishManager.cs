using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
using DG.Tweening;

public class FinishManager : LocalSingleton<FinishManager>
{
    public void Ambulance(GameObject ambulance)
    {
        if (Player.Instance.player.patient.Count >= 2)
        {
            for (int i = 0; i < 2; i++)
            {
                STD(ambulance);
            }

        }
        else if (Player.Instance.player.patient.Count >= 1)
        {
            STD(ambulance);
            GameManager.Instance.GameFinish();
        }
        else
        { 
            GameManager.Instance.GameFinish();
        }
    }
    static void STD(GameObject ambulance)
    {   
        Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.GetChild(0).GetComponent<AnimancerComponent>().Stop();
        Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.SetParent(null);
       // Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.DORotate(new Vector3(-90, 0, 180), .58f);
        Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.DOMove(ambulance.transform.GetChild(1).transform.position + new Vector3(0, 1.5f, 0), .7f);
        Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].transform.DOMove(ambulance.transform.position + new Vector3(0, 1.5f, 0), .7f).SetDelay(.7f);
        Player.Instance.player.patient[Player.Instance.player.patient.Count - 1].gameObject.GetComponent<Rigidbody>().isKinematic = false;
        Player.Instance.player.patient.Remove(Player.Instance.player.patient[Player.Instance.player.patient.Count - 1]);
    }
}
