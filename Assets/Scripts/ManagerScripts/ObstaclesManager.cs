using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObstaclesManager : LocalSingleton<ObstaclesManager>
{
    public Obstacles obstacles;
    public ObstacleParticles particle;

    public void Obstacle1(GameObject patient)
    {
        for (int i = 0; i < Player.Instance.player.patient.Count; i++)
        {
            if (Player.Instance.player.patient[i] == patient)
            {
                obstacles.lastIndex = i;

                break;
            }
        }
        for (int a = Player.Instance.player.patient.Count; a > obstacles.lastIndex; a--)
        {
            STD(Player.Instance.player.patient[a - 1]);
            RemoveList();
        }

        //else
        //{
        //    obstacles.lose = true;
        //}
    }
    public void Obstacle2()
    {
        for (int i = Player.Instance.player.patient.Count; i > Player.Instance.player.patient.Count - 6; i--)
        {
            STD(Player.Instance.player.patient[i - 1]);
            if (Player.Instance.player.patient.Count <= i)
            {
                obstacles.lose = true;
                break;
            }
            RemoveList();
        }
    }

    public void Obstacle3()
    {
        StartCoroutine(Ob3());
    }
    IEnumerator Ob3()
    {
        while (!obstacles.loopExit)
        {
            try
            {
                STD(Player.Instance.player.patient[Player.Instance.player.patient.Count-1]);
                //if (i < particle.obstacle3.Length)
                //{
                //    particle.obstacle3[i].Play();
                //}
                RemoveList();
            }
            catch (System.Exception)
            {
                break;
                throw;
            }
            yield return new WaitForSeconds(.4f);
        }
        obstacles.loopExit = false;
    }
    public void Obstacle4(GameObject patient, GameObject hitObj)
    {
        for (int i = 0; i < Player.Instance.player.patient.Count; i++)
        {
            if (Player.Instance.player.patient[i] == patient)
            {
                obstacles.lastIndex = i;

                break;
            }
        }
        for (int a = Player.Instance.player.patient.Count; a > obstacles.lastIndex; a--)
        {
            STD(Player.Instance.player.patient[a - 1], true);
            //Collider[] collider = Player.Instance.player.patientLower[a - 1].GetComponentsInChildren<Collider>();
            //foreach (Collider col in collider)
            //{
            //    col.enabled = false;
            //}
            Player.Instance.player.patient[a - 1].transform.DORotate(new Vector3(-90,0,-90),.4f);
            // Player.Instance.player.patient[a-1].transform.DOScale(new Vector3(.3f, .3f, .3f), .3f);
            Player.Instance.player.patient[a - 1].transform.DOMove(hitObj.transform.GetChild(0).transform.position, .4f);
            Player.Instance.player.patient[a - 1].transform.DOMove(hitObj.transform.GetChild(1).transform.position, .4f).SetDelay(.4f);
            RemoveList();
        }
    }
    void RemoveList()
    {
        Player.Instance.player.patient.Remove(Player.Instance.player.patient[Player.Instance.player.patient.Count - 1]);
        PlayerMove.Instance.move.patient2.Remove(PlayerMove.Instance.move.patient2[PlayerMove.Instance.move.patient2.Count - 1]);
    }
    void STD(GameObject obj,bool value = false)
    {
        //Stretcher.Instance._stretcher.positionPlus -= Stretcher.Instance._stretcher.plus;
        obj.transform.DOKill();
        obj.transform.SetParent(null);
        obj.GetComponent<Rigidbody>().isKinematic = value;
       // obj.transform.GetChild(0).GetComponent<Rigidbody>().isKinematic = value;
        //if (Stretcher.Instance._stretcher.patientLineUp == 0)
        //{
        //    Stretcher.Instance._stretcher.patientLineUp = 1;
        //}
        //else
        //{
        //    Stretcher.Instance._stretcher.patientLineUp = 0;
        //}

    }
}
[System.Serializable]
public class Obstacles
{
    public bool lose = false;
    public GameObject obstacle_1;
    public GameObject obstacle_2;
    public GameObject obstacle_3;
    public GameObject obstacle_4;
    public bool loopExit;
    public int lastIndex;
}
[System.Serializable]
public class ObstacleParticles
{
    //public ParticleSystem[] obstacle3;
}