using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : LocalSingleton<Player>
{
    public PlayerInput player;
    public bool finish= false;
    private void Start()
    {
        PlayerIdle();
        AnimationManager.Instance.anim.stretcher.Stop();
        
    }
    public void PlayerIdle()
    {
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.doctor1, AnimationManager.Instance.anim.doctorIdle,1,.5f);
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.doctor2, AnimationManager.Instance.anim.doctorIdle,1,.5f);
    }
    public void PlayerRun()
    {
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.doctor1, AnimationManager.Instance.anim.doctorRun,.9f,.5f);
        AnimationManager.Instance.PlayAnim(AnimationManager.Instance.anim.doctor2, AnimationManager.Instance.anim.doctorRun,.9f,.5f);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("patient"))
        {
            UIManager.Instance.FeverInvoke();
            player.stars.Play();
            Stretcher.Instance.SPStart(other.gameObject);
            PlayerMove.Instance.move.patient2.Add(other.gameObject);
            player.patient.Add(other.gameObject);
        }
        else if (other.CompareTag("Ambulance"))
        {
            if (player.patient.Count >= 1)
            {
                player.�ndex++;
                player.perfect.Play();
                UIManager.Instance.TMPXCount(player.�ndex.ToString());
            }
            other.gameObject.GetComponent<Collider>().enabled = false;
            FinishManager.Instance.Ambulance(other.gameObject);
        }
        else if (other.CompareTag("Obstacles-2") && gameObject.CompareTag("Player"))
        {
            Stretcher.Instance.SteretcherHit();
            PlayerMove.Instance.move.speed = PlayerMove.Instance.move.speed / 1.5f;
            ObstaclesManager.Instance.Obstacle2();
        } 
        else if (other.CompareTag("Obstacles-3") && gameObject.CompareTag("Player"))
        {
            AnimationManager.Instance.CameraShape(AnimationManager.Instance.anim.cameraShape);
            OB3();
        }
        
        else if (other.CompareTag("Finish"))
        {
            finish = true;
            Camera.main.gameObject.GetComponent<Animator>().enabled = false;
            player.speedForce.Play();
            UIManager.Instance.Finish();
            CameraManager.Instance.target = CameraManager.Instance.camPos4;
            PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed +5;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Obstacles-3") && gameObject.CompareTag("Player"))
        {
            if (player.patient.Count == 0)
            {
                GameManager.Instance.GameLose();
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Obstacles-3") && gameObject.CompareTag("Player"))
        {
            AnimationManager.Instance.anim.camera.Stop();
            Stretcher.Instance.SteretcherRun();
            PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed;
            ObstaclesManager.Instance.obstacles.loopExit = true;
        }else if (other.CompareTag("Obstacles-2") && gameObject.CompareTag("Player"))
        {
            Stretcher.Instance.SteretcherRun();
            PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed;
        }
    }
    void OB3()
    {
        Stretcher.Instance.SteretcherHit();
        PlayerMove.Instance.move.speed = PlayerMove.Instance.move.memorySpeed / 1.5f;
        ObstaclesManager.Instance.Obstacle3();
        ObstaclesManager.Instance.obstacles.loopExit = false;
    }
}
[System.Serializable]
public class PlayerInput
{
    //Player
    public List<GameObject> patient;
    public ParticleSystem stars;
    public ParticleSystem perfect;
    public ParticleSystem speedForce;
    public ParticleSystem heal;
    public int �ndex;
}