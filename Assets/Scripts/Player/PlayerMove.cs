using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMove : LocalSingleton<PlayerMove>
{
    public PlayerMoveInput move;

    private float lastMousePosX;
    bool isRotating;
    [Header("MoverSettings")]
    [Space]
    [SerializeField] float moveSpeed = .1f;
    [SerializeField] float moveRange = 5f;
    [SerializeField] float moveUnit = 1f;
    Coroutine MoveCoroutine;
   

    private void Update()
    {
        if (gameObject.transform.GetChild(0).transform.rotation.eulerAngles.z > 40 || gameObject.transform.GetChild(0).transform.rotation.eulerAngles.z < -40)
        {
            gameObject.transform.GetChild(0).transform.DORotate(Vector3.zero,.2f);
            gameObject.transform.GetChild(1).transform.DORotate(Vector3.zero,.2f);
            gameObject.transform.GetChild(2).transform.DORotate(Vector3.zero,.2f);
        }
        if (!Player.Instance.finish)
        {
            Mover();
        }
        else
        {
            transform.DOMove(new Vector3(0, transform.position.y, transform.position.z + move.speed * Time.deltaTime), .001f);
        }
    }
    void Mover()
    {
        transform.DOMove(new Vector3(transform.position.x, transform.position.y, transform.position.z + move.speed * Time.deltaTime), .001f);
        if (Input.GetMouseButtonDown(0))
        {
            UIManager.Instance.TapToStart();
            lastMousePosX = Input.mousePosition.x;
            isRotating = true;
        }
        else if (isRotating && Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x - lastMousePosX > 0)
            {
                if (MoveCoroutine == null)
                {
                    //Slope(-.15f);
                    MoveRoutine(true);
                }
            }
            else if (Input.mousePosition.x - lastMousePosX < 0)
            {
                if (MoveCoroutine == null)
                {
                    //Slope(.15f);
                    MoveRoutine(false);
                }
            }
            else
            {
                //SlopeReset();
                //move.turnSpeed2 = 0;
                //transform.DORotate(new Vector3(transform.rotation.x, 0, transform.rotation.z), move.turnSmooth);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isRotating = false;
        }
    }
    void MoveRoutine(bool isRight)
    {

        if (isRight)
        {
            if (transform.position.x < moveRange)
            {
                transform.DOMoveX(transform.position.x + moveUnit * Time.deltaTime, moveSpeed);
            }
        }
        else
        {
            if (transform.position.x > -moveRange)
            {
                transform.DOMoveX(transform.position.x - moveUnit * Time.deltaTime, moveSpeed);
            }
        }
        lastMousePosX = Input.mousePosition.x;
        MoveCoroutine = null;
    }
    void Slope(float slope)
    {
        for (int i = 0; i < Player.Instance.player.patient.Count; i++)
        {
            float x = Stretcher.Instance.gameObject.transform.GetChild(0).gameObject.transform.position.x;
            move.patient2[i].transform.DOMoveX(x + slope * i, .5f);
        }
    }
    void SlopeReset()
    {
        for (int i = 0; i < Player.Instance.player.patient.Count; i++)
        {
            float x = Stretcher.Instance.gameObject.transform.GetChild(0).gameObject.transform.position.x;
            move.patient2[i].transform.DOMoveX(x, .5f);
        }
    }



}
[System.Serializable]
public class PlayerMoveInput
{
    //PlayerMove
    public List<GameObject> patient2;
    public float speed;
    public float memorySpeed;
    [HideInInspector] public float turnSpeed2;
    public float turnSpeed;
    public float turnSmooth;
    public float turnRotation;
    public bool firstInpur;
}